import Vue from 'vue'
import Router from 'vue-router'
import Employee from '../components/employee/Employee.vue'
import EmpsList from '@/components/employee/EmpsList'
import Login from '@/components/auth/Login'
import SkillsList from '@/components/skill/SkillsList'
import Skill from '@/components/skill/Skill'
import Main from '@/components/Main'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  let user
  if (localStorage.getItem('loggedUser')) {
    try {
      user = JSON.parse(localStorage.getItem('loggedUser'))
    } catch (e) {
      localStorage.removeItem('registeredUsers')
    }
  }
  if (!user) {
    next()
    return
  }
  next('/main/emplist')
}

const ifAuthenticated = (to, from, next) => {
  let user
  if (localStorage.getItem('loggedUser')) {
    try {
      user = JSON.parse(localStorage.getItem('loggedUser'))
    } catch (e) {
      localStorage.removeItem('registeredUsers')
    }
  }
  if (user) {
    next()
    return
  }
  next('/main/emplist')
}

export default new Router({
  routes: [
    {
      path       : '/main',
      name       : 'Main',
      component  : Main,
      beforeEnter: ifAuthenticated,
      children   : [
        {
          path       : 'emplist',
          name       : 'EmpList',
          component  : EmpsList,
          beforeEnter: ifAuthenticated
        },
        {
          path       : 'skillcard/:id',
          name       : 'Skill',
          component  : Skill,
          beforeEnter: ifAuthenticated
        },
        {
          path       : 'employees/:id',
          name       : 'Employees',
          component  : Employee,
          beforeEnter: ifAuthenticated
        },
        {
          path       : 'skillslist',
          name       : 'Skills',
          component  : SkillsList,
          beforeEnter: ifAuthenticated
        }
      ]
    },
    {
      path       : '/',
      name       : 'Auth',
      component  : Login,
      beforeEnter: ifNotAuthenticated
    },
    {
      path       : '/skillslist/:skillid',
      name       : 'Skill',
      component  : Skill,
      beforeEnter: ifAuthenticated
    }
  ]
})
