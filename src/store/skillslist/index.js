/* eslint-disable */
const skillsList = {
  state: {
    skills: []
  },
  mutations: {
    addSkill (state, payload) {
      // id already generated in service
      state.skills.push(payload.data)
    },
    editSkill (state, payload) {
      for (let i = 0; i < state.skills.length; i++){
        if (state.skills[i].id == payload.data.id){
          state.skills[i] = payload.data;
          break;
        }
      }
    },
    deleteSkill(state, payload){
      for (let i = 0; i < state.skills.length; i++){
        if (state.skills[i].id == payload.data.id){
          state.skills.splice(i,1);
          break;
        }
      }
    }
  },
  actions: {},
  getters: {
    getSkills: state => state.skills
  }
}
export default skillsList
