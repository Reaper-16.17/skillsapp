import Vue from 'vue'
import Vuex from 'vuex'
import employeesList from '@/store/employeeslist'
import skillsList from '@/store/skillslist'
import user from '@/store/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    employeesList,
    skillsList,
    user
  }
  // state: {
  //   registeredUsers: [],
  //   skillsArr: [],
  //   employeesArr: []
  // },
  // mutations: {
  //   addNewUser (state, payload) {
  //     state.registeredUsers.push({
  //       name: payload.name,
  //       login: payload.login,
  //       password: payload.password
  //     })
  //   },
  //   setSkills () {
  //     //   url: '',
  //     //   git: '',
  //     //   connectedCategories: [],
  //     //   description: '',
  //     this.state.skillsArr = [
  //       {id: 1, title: 'HTML', type: 'Technology', category: 'Frontend', version: 5, employees: 20, projects: 5, tags: ['Markup'], url: 'URL', git: 'GIT', connectedCategories: [], description: 'Lorem ipsum...'},
  //       {id: 2, title: 'CSS', type: 'Technology', category: 'Frontend', version: 2, employees: 20, projects: 5, tags: ['Markup'], url: 'URL', git: 'GIT', connectedCategories: [], description: 'Lorem ipsum...'},
  //       {id: 3, title: 'JS', type: 'Technology', category: 'Frontend', version: 3, employees: 20, projects: 5, tags: ['Markup'], url: 'URL', git: 'GIT', connectedCategories: [], description: 'Lorem ipsum...'},
  //       {id: 4, title: 'HTML', type: 'Technology', category: 'Frontend', version: 4, employees: 20, projects: 5, tags: ['Markup'], url: 'URL', git: 'GIT', connectedCategories: [], description: 'Lorem ipsum...'},
  //       {id: 5, title: 'HTML', type: 'Technology', category: 'Frontend', version: 6, employees: 20, projects: 5, tags: ['Markup'], url: 'URL', git: 'GIT', connectedCategories: [], description: 'Lorem ipsum...'},
  //       {id: 6, title: 'HTML', type: 'Technology', category: 'Frontend', version: 1, employees: 20, projects: 5, tags: ['Markup'], url: 'URL', git: 'GIT', connectedCategories: [], description: 'Lorem ipsum...'},
  //       {id: 7, title: 'HTML', type: 'Technology', category: 'Frontend', version: 3, employees: 20, projects: 5, tags: ['Markup'], url: 'URL', git: 'GIT', connectedCategories: [], description: 'Lorem ipsum...'},
  //       {id: 8, title: 'HTML', type: 'Technology', category: 'Frontend', version: 2, employees: 20, projects: 5, tags: ['Markup'], url: 'URL', git: 'GIT', connectedCategories: [], description: 'Lorem ipsum...'}
  //     ]
  //   },
  //   setEmployees () {
  //     this.state.employeesArr = [
  //       { id: 1, name: 'John', surname: 'Doe', position: 'Developer', project: 'Test project', department: 'Test department', skills: ['JavaScript', 'React', 'HTML', 'CSS'] },
  //       { id: 2, name: 'Jack', surname: 'Reaper', position: 'Reaper', project: 'Reaper project', department: 'Test department', skills: ['Java', 'Spring', 'Hibernate'] },
  //       { id: 3, name: 'John', surname: 'Doe', position: 'Developer', project: 'Test project', department: 'Test department', skills: ['PHP', 'MySQL', 'Apache'] },
  //       { id: 4, name: 'John', surname: 'Doe', position: 'Developer', project: 'Test project', department: 'Test department', skills: ['Docker', 'Kubernetes'] },
  //       { id: 5, name: 'John', surname: 'Doe', position: 'Developer', project: 'Test project', department: 'Test department', skills: [] },
  //       { id: 6, name: 'John', surname: 'Doe', position: 'Developer', project: 'Test project', department: 'Test department', skills: [] }
  //     ]
  //   },
  //   addSkill (state, payload) {
  //     state.skillsArr.push(payload.data)
  //   },
  //   editSkill (state, payload) {
  //     state.skillsArr = payload.data
  //   },
  //   deleteSkill (state, payload) {
  //     state.skillsArr = payload.data
  //   }
  // },
  // getters: {
  //   getSkillsArr: state => state.skillsArr,
  //   getEmployeesArr: state => state.employeesArr
  // }
})
