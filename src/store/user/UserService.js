export default class UserService {
  static login (username, password) {
    return {}
  }
  static logout () {
    return {}
  }
  static restore (username) {
    return {}
  }
  static createNewSkill (skillData) {
    return {}
  }
  static editSkill (skillId, newSkillData) {
    return {}
  }
  static deleteSkill (skillId) {
    return {}
  }
  static createNewUser (userData) {
    return {}
  }
  static editUser (userId, newUserData) {
    return {}
  }
  static deleteUser (userId) {
    return {}
  }
}
