const user = {
  state: {
    loggedUser: null,
    isAuthenticated: false
  },
  mutations: {
    setAuth (state, payload) {
      state.isAuthenticated = payload.isAuthenticated
    },
    setLoggedUser (state, payload) {
      state.loggedUser = payload.user
    }
  },
  actions: {},
  getters: {
    getLoggedUser: state => state.loggedUser,
    getAuth: state => state.isAuthenticated
  }
}
export default user
