/* eslint-disable */
const employeesList = {
  state: {
    employees: []
  },
  mutations: {
    addEmployee (state, payload) {
      // id already generated in service
      state.employees.push(payload.data)
    },
    editEmployee (state, payload) {
      for (let i = 0; i < state.employees.length; i++){
        if (state.employees[i].id == payload.data.id){
          state.employees[i] = payload.data;
          break;
        }
      }
    },
    deleteEmployee(state, payload){
      for (let i = 0; i < state.employees.length; i++){
        if (state.employees[i].id == payload.data.id){
          state.employees.splice(i,1);
          break;
        }
      }
    }
  },
  actions: {},
  getters: {
    getEmployees: state => state.employees
  }
}
export default employeesList
